import pyomo.environ as pyo
SnapesRiddle = pyo.AbstractModel()

SnapesRiddle.noBottles = pyo.Param(domain=pyo.NonNegativeIntegers, initialize=7)

SnapesRiddle.positions  = pyo.RangeSet(1, SnapesRiddle.noBottles)
SnapesRiddle.contents   = pyo.Set(initialize=['BlackFirePotion', 'PurpleFirePotion', 'Wine', 'Poison'])
SnapesRiddle.noContents = pyo.Param(SnapesRiddle.contents,
                                    initialize={'BlackFirePotion' : 1, 'PurpleFirePotion' : 1, 'Wine' : 2, 'Poison' : 3})

SnapesRiddle.giant = pyo.Set(within=SnapesRiddle.positions)
SnapesRiddle.dwarf = pyo.Set(within=SnapesRiddle.positions)

SnapesRiddle.Bottles = pyo.Var(SnapesRiddle.contents, SnapesRiddle.positions, domain=pyo.Binary, name="Bottles")


@SnapesRiddle.Constraint(SnapesRiddle.contents, name="contentDistribution")
def contentDistribution(model, cont):
    return sum(model.Bottles[cont, pos] for pos in model.positions) == model.noContents[cont]

@SnapesRiddle.Constraint(SnapesRiddle.positions, name="oneContentPerBottle")
def oneContentPerBottle(model, pos):
    return sum(model.Bottles[cont, pos] for cont in model.contents) == 1


@SnapesRiddle.Constraint(SnapesRiddle.positions, name="firstHintA")
def firstHintA(model, pos):
    if pos != 1:
        return model.Bottles['Poison', pos-1] >= model.Bottles['Wine', pos]
    else:
        return  pyo.Constraint.Skip
@SnapesRiddle.Constraint(name="firstHintB")
def firstHintB(model):
    return model.Bottles['Wine', 1] == 0

@SnapesRiddle.Constraint(SnapesRiddle.contents, name="secondHintA")
def secondHintA(model, cont):
    if cont=="BlackFirePotion":
        return model.Bottles['BlackFirePotion', 1] + model.Bottles['BlackFirePotion', 7] == 0
    else:
        return model.Bottles[cont, 1] + model.Bottles[cont, 7] <= 1

@SnapesRiddle.Constraint(SnapesRiddle.dwarf, SnapesRiddle.giant, name="thirdHint")
def thirdHint(model, d, g):
    return model.Bottles['Poison', d] + model.Bottles['Poison', g] == 0


@SnapesRiddle.Constraint(SnapesRiddle.contents, name="fourthHint")
def fourthHint(model, cont):
    return model.Bottles[cont, 2] == model.Bottles[cont, 6]

SnapesRiddle.uniquenessCuts = pyo.ConstraintList()

SnapesRiddle.obj = pyo.Objective(expr = 0)

solver = pyo.SolverFactory('scip')

it = 0
for giant in range(1, 8):
    for dwarf in range(giant + 1, 8):
        instance = SnapesRiddle.create_instance({None: {
            'giant':{None: [giant]},
            'dwarf':{None: [dwarf]}
        }})
        results = solver.solve(instance)

        print(f"For giant at {giant} and dwarf at {dwarf}, the solver returns {results.solver.status} and {results.solver.termination_condition}")
        if results.solver.termination_condition != 'infeasible':
            blackFire = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['BlackFirePotion', i])) == 1][0]
            purpleFire = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['PurpleFirePotion', i])) == 1][0]
            wines = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['Wine', i])) == 1]
            poisons = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['Poison', i])) == 1]
            print(f"BlackFirePotion at {blackFire},\n",
            f"PurpleFirePotion at {purpleFire},\n",
                  f"Wine at {wines[0]} & {wines[1]},\n",
                  f"Poison at {poisons[0]}, {poisons[1]}, & {poisons[2]}\n")
        else:
            print(f"\n")

        while(results.solver.termination_condition != 'infeasible'):
            ## cut off current solution and resolve
            cutExpr = instance.Bottles['BlackFirePotion', blackFire] + instance.Bottles['PurpleFirePotion', purpleFire]
            cutExpr += instance.Bottles['Wine', wines[0]] + instance.Bottles['Wine', wines[1]]
            cutExpr += instance.Bottles['Poison', poisons[0]] + instance.Bottles['Poison', poisons[1]] + instance.Bottles['Poison', poisons[2]]
            instance.uniquenessCuts.add(cutExpr<=6)

            results = solver.solve(instance)
            blackFire = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['BlackFirePotion', i])) == 1][0]
            purpleFire = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['PurpleFirePotion', i])) == 1][0]
            wines = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['Wine', i])) == 1]
            poisons = [i for i in range(1, 8) if round(pyo.value(instance.Bottles['Poison', i])) == 1]
            if results.solver.termination_condition != 'infeasible':
                print(f"{'Alternative Solution Found'.upper()}:\nBlackFirePotion at {blackFire},\n",
                      f"PurpleFirePotion at {purpleFire},\n",
                      f"Wine at {wines[0]} & {wines[1]},\n",
                      f"Poison at {poisons[0]}, {poisons[1]}, & {poisons[2]}\n")

        # with open(f"intermedResultSnape_{it}.txt", 'w') as f:
        #     instance.pprint(f)
        print(f"\n\n")
        it += 1

